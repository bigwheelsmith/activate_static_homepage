function smooth_scroll(section){
    document.querySelector(section).scrollIntoView({ 
        behavior: 'smooth' 
      });
}